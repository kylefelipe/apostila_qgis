# Mãos à Massa

Agora que já foi apresentado ao Qgis, vamos começar operar e ver suas funcionalidades e a capacidade de adaptar o Qgis para o uso diário, mas, antes, uma pequena explicação sobre os tipos de dados em SIG.  

Básicamente, em SIG, temos 3 tipos de informação:

*   Dados Vetoriais (Ponto, linha, poligono/área).
*   Dados Matriciais (rasters).
*   Dados Tabulares (não possuem geometria ou georreferenciamento, mas podem ser georreferenciados).

Para cada um dos tipos de dados acima, possuímos os mais variados formatos de dados alguns bem conhecidos e outros tão anõnimos quanto figurantes em um filme cult Iraniano, tanto formatos abertos e livres quanto formatos fechados e proprietários.
O Qgis, por ser um software livre presa por trabalhar, por padrão, com formatos livres, e para alguns formatos fechados, é necessário adquirir alguma licensa para se ter o driver correto.

Mas o que é esse formato de dados que tanto se fala aqui.  
É possível escrever uma enciclopédia inteira sobre esse assunto, mas vou reduzir bastante, a exemplos simples.

## Dados Vetoriais:

Para trabalhar com vetores, os formatos de dados mais utilizados são:

#### [SHAPEFILE](http://www.esri.com/library/whitepapers/pdfs/shapefile.pdf):
Foi um dos primeiros formatos criados para se trabalhar com vetores, desenvolvido pela ESRI, a mesma desenvolvedora do ARCGIS, tendo a data da primeira documentação de 2008, com pouqíssimas evoluções nesses 20 anos de vida. A empresa abriu o formato para a comunidade.  
Formado por 3 arquivos obrigatórios e um opcional, sendo eles:

*   .shp - É o arquivo que guarda a geometria, o desenho. Obrigatório.
*   .dbf - É um arquivo de banco de dados bem arcaico e limitado, funciona, basicamente como uma planilha, guardando apenas uma linha e não faz relação com nada. Obrigatório.
*   .shx - É o arquivo que faz a relação entre o vetor no arquivo .shp com os dados dele no arquivo .dbf. Obrigatório.
*   .prj - o arquivo prj é nada mais que um arquivo de texto contendo os dados da projeção dos dados do shapefile, pode até mesmo ser alterado com algum editor de texto comun. É um arquivo opcional, jaá que nos primórdios do SIG, ao inserir os dados em um software, o prj era configurado manualmente, nem todo software lia esse arquivo.  

Sempre ao se repassar um arquivo shapefile para alguém deve-se informar a codificação da tabela de atributos, para não haja problemas ao abrir e editar os dados, e não é possível misturar os tipos de vetores no SHP, sendo necessário ter um arquivo para cada tipo de geometria.  

#### [KML](https://developers.google.com/kml/):
É um formato de arquivos desenvolvido pela GOOGLE muito utilizado em seu software GOOGLE EARTH, e ficou bem popular devido a simplicidade. KML é a sigla de Keyhole Markup Language, e nada mais é que um arquivo de texto com os dados organizados por tags lá dentro, aqueles que conhecem a linguagem, conseguem montar um arquivo utilizando um editor de texto.  

#### [Geopackage](www.geopackage.org):
Esse é um arquivo que já nasceu livre, aberto e amado. Criado pela OSGEO como um formato padrão para armazenar e distribuir dados espaciais, funciona bem parecido com um banco de dados, mas de uma forma bem mais simples, permitindo que possa conter mais de uma camada dentro dele, separado em tabelas, e as tabelas podem ter tipos de geometria diferentes entre si, mas cada tabela só pode ter um tipo de geometria e uma coluna de geometria também, esse garoto é bem ecletico, continuaremos a comprovar isso já já.

## Dados Matriciais:

Para se trabalhar com dados matriciais os formatos utilizados são:

#### [GeoTiff](http://www.gdal.org/frmt_gtiff.html):
Esse é o formato mais famoso para se trabalhar com dados matriciais (rasters), como imagens de satélite, radar e etc. Possui internamente as informações espaciais necessárias. é um arquivo Tiff, com coordenadas.

#### [Geopackage](www.geopackage.org):
Nossa, é possivel salvar raster dentro de um geopakage também?  
Sim, é possivel salvar arquivos aqui dentro, eu avisei que o rapaz é ecletico.

## Dados Tabulares:

Para se trabalhar com dados tablulares, os formatos mais utilizados são:

#### [CSV](https://pt.wikipedia.org/wiki/Comma-separated_values):

O arquivo CSV é um arquivo de texto, onde os dados lá dentro são determinados por algum caractere delimitador (virgua, ponto e vírgula, dois pontos, espaço).  
Por tabulação, ou por tamanho fixo.

#### Planilhas
(falhou, não achei referência bonitinha para planilhas):
Planilhas são OS MAIS utilizados meios de de distribuir dados tabulares, seja ela produzida no [MS Office](https://pt.wikipedia.org/wiki/Microsoft_Office) ou no [Open Office](https://pt.wikipedia.org/wiki/OpenOffice.org) e seus irmãos.

#### [Geopackage](www.geopackage.org):
Nossa!!! Geopackage aceita dados tabulares também?  
Siiiimmm... O geopackage possui um banco de dados portável dentro dele, chamado [SQLITE](www.sqlite.org), ele em seus poucos anos de vida, tem recebido grandes contribuições devido a sua natureza Livre e Aberta, sendo desenvolvido colaborativamente pelos amantes do FOSS (essa fica para outra hora). Você também pode contribuir de várias maneiras para o projeto. Acesse o site, e seja feliz.

Explica mais?  
Calma jovem gafanhoto, existe um universo de formatos que podemos ver depois, como os geoserviços e banco de dados. Mas isso pode ser de mais para você que está começando agora.  
Vamos começar a trabalhar, por quê vetores, rasters e dados tabulares não irão se analizar e se manipularem sozinhos.
