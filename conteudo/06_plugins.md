# Complementos (plugins)

Umas das propriedades do Qgis que o popularizou foi a capacidade de ser customizado, e os plugins são uma forma de customizar ele.  
Plugins para o qgis podem ser feitos em uma linguagem bem simples, já falada aqui, o [__Python__](www.python.org), mas não menosprese ele por sua simplicidade, o poder dessa linguagem "é mais de oito mil".  
O menu _`Plugins`_ possui os plugins que foram instalados pelo usuário no qgis, o console para utilizarmos python dentro do qgis, e o Gerenciador de plugins.

![](imagens/plugins_1.png)

## Gerenciador de Complementos:##

O gerenciador de complementos nos permite instalar, remover plugins, adicionar e remover repositórios.  
Ele tambem possui um painel lateral com as seguintes abas:
*   Todos (All)
*   Instalados (Installed)
*   Atualizável (Upgradeable) - Esse só aparece se possuir plugins que precisa de atualização instalados.
*   Novo (New) - Esse só aparece se se ouver novos plugins no repositório
*   Configurações (Settings)

#### Todos (All)####

![](imagens/plugins_2.png)

Por aqui podemos ver os plugins, todos juntos e misturados, instalados ou não, disponíveis em todos os repositórios configurados.  
Podemos procurar pelos plugins digitando em Procurar (Search)  

![](imagens/plugins_03.png): Todos os plugins com esse icone na lista não estão instalados.  
Caso não consiga encontrar um plugin instalado é pelo motivo dele não estar habilitado. Sim é possivel abilitar ou desabilitar um plugin.  
Basta marcar/desmarcar a caixa que encontra-se do lado do plugin para abilita/desabilitar o mesmo.  

![](imagens/plugins_04.png) Esse é um plugin abilitado.
![](imagens/plugins_05.png) Esse é um plugin desabilitado.

Alguns plugins já vem instalados por padrão no qgis e vem desabilitados também, basta macar a caixa.

Para instalar um plugin é muito, mas muito difícil de se fazer.  
Após encontrar o plugin na lista, basta clicar sobre ele, e em seguida clicar em __Instalar__ __Plugin__.

![](imagens/plugins_06.png)

Dificil né!

#### Configurações (Settings)####

Aqui vamos configurar algumas coisas referente aos plugins.
Ja procurou um plugin que todos te mostraram e ele não aparece na sua lista, é aqui que vamos mudar isso.  

![](imagens/plugins_07.png)

Aqui configuramos a frequencia com a qual o Qgis procura por atualizações dos plugins.  
Mostrar plugins experimentais, alguns plugins ainda não estão totalmente confiáveis e nem terminados e por padrão o Qgis omite esses plugins para que um usuário menos experiente tenha problemas com ele, esse pode ser um motivo do plugin que você tanto procura não aparecer na listagem de plugins.  
Mostrar plugins obsoletos, esses plugins já não são mais mantidos pelos seus desenvolvedores e não estão mais em modo produção por alguma incompatibilidade, deve-se tomar muito cuidado com eles justamente por isso, pode ser outro motivo do desejado plugin não aparecer na lista.  

> Aprecie com moderação.  

Configurar e remover repositórios
