# Arquivos de Texto Delimitado (CSV)

> \- Faz um mapa dessa propriedade pra mim?  
> \- Ok, tem os vetores dela?  
> \- Tenho esse memorial descritivo com as coordenadas.  
> \- ...

É também muito comum utilizarmos arquivos, como planilhas, arquivos de texto, documentos, com coordenadas e precisamos desenhar esses pontos no Qgis.  
Para isso precisamos saber:

 Tipo de coordenada:

-   Geográfica: GMS - Grau Minutos e Segundos, DD - Grau Decimal
-   Plana: UTM - Universal Transversa de Mercator - Em Metros

Em SIG  temos um padrão para obedecermos ao informar as coordenadas nos sistemas:  

*   Sempre informe na ordem: Longitude (X) e Latitude (Y).  

O Qgis tem um padrão para leitura de coordenadas:  

*   Geográficas - GMS:  
 O padrão para Coordenadas GMS no QGIS é GGG MM SS.DDD.
 Ou seja GRAU separado de Minutos separado de Segunso por ESPAÇO, e o separador de decima é o ponto/virgula,
 e o indicador de hemisfério é o sinal de '-', e não as letras.

*   Geográficas - DD:  
 O padrão para Coordenadas DD no QGIS é GGG.DDDDD  
 Ou deseja, Grau separado de Decimal por ponto/vírgula.

*   UTM:
  O padrão de coordenadas UTM no Qgis é MMMMMM.DDDDD  
  Ou seja, Metros separado de Decimal por ponto/vírgula.

Perceba que em nenhum momento é utilizado separador de milhar.  

Antes de adicionar os dados ao Qgis temos de verificar qual o tipo de coordenada, se há coordenadas com separador de decimal misturando ponto e virgula.
Remover separadores de milhar, caso haja.

Coordenadas UTM devemos ter a informação de FUSO (__NÃO CONFUNDA COM FUSO HORÁRIO__)  

Após as verificações, podemos montar o nosso texto delimitado para inserir no QGIS (é aqui que a sanidade do estagiário é testada).  

O Arquivo de texto delimitado é um arquivo onde os dados estão estruturados conforme um padrão, para isso utilizamos um DELIMITADOR, que irá separar os campos lá dentro.  
Esse delimitador deve ser bem escolhido para não haver problemas, como separar as casas decimais de um dado, por exemplo.  
Suites de escritório como o Open/Libre/WPS Oficce (CALC, MATH, Spreadsheets) (Livres) e MS Oficce (Excel) (Proprietária) tem a capacidade de salvar os arquivos no formato CSV - Comma-Separeated Values, Valores Separados por Vírgula, mas nada nos impede de abrir um simples editor de texto e montar o nosso próprio arquivo.

![](imagens/txtdelimitado_01)  

Podemos ver na imagem acima, um arquivo de texto, onde:  
 Podemos perceber que os dados são separados por ';'.  
 A primeira linha contém o nome dos campos (vertice, x, y).  
 As coordenadas estão em UTM.
 O separador decimal das coordenadas é a vírgula.  

![](imagens/txtdelimitado_02)  

Na imagem acima as coordenadas estão em uma estrutura um pouco diferente:
 Dados são separados por vírgula ','.
 Coordenadas em DD.
 Separador decima é o ponto '.'.
 Indicação de hemisfério é a presença do sinal de subtração '-'.  

![](imagens/txtdelimitado_03)  

Na imagem acima as cooredenadas estão em GMS.

## Abrindo o arquivo DELIMITADO

Para poder vermos as coordenadas no mapa, vamos precisamos importar os dados no QGIS.  
Os passos são bem parecios com a importação de dados tabulares.


![](imagens/addarquivos_12.png)  

A diferença está na configuração do dado:

### Coordenadas DD:

![](imagens/txtdelimitado_04.png)  

Para as coordenadas em Grau Decimal, deixamos desmarcada a opção __Coordenadas GMS__ (__DMS COORDINATES__), repare nas configurações em __Formato do Arquivo__ (__File format__)  
O QGIS verifica o nome dos campos e já faz a configuração dos campos 'Campo X' (X field) e 'Campo Y' (Y field), caso ele não reconheça, ou estiver com outro nome, basta indicar na lista.  

### Coordenadas GMS

![](imagens/txtdelimitado_05.png)  

Nesse caso, marcamos a opção __Coordenadas GMS__.  
__Atenção:__ O separador de decimal nesse arquivo é a virgula, nesse caso, marcamos a opção __VIRGULA__ __COMO__ __SEPARADOR DE DECIMAL__ (__Decimal__ __Separator__ __is__ __comma__)


### Coordenadas UTM

![](imagens/txtdelimitado_06.png)  

Nesse caso, o que difere dos outros, básicamente, é o DATUM, pois as coodenadas estão em UTM.  

Após a importação do dados, talvez seja necessário acertar o Sistema de Referência de Coordenada - SRC da camada.

Para isso, basta clicar com o botão direito sobre a camada _`Propriedades (Properties)`_:  

![](imagens/txtdelimitado_07.png)  

Na janela que abrir, vá em __GERAL (GENERAL)__, em em __Sistema de Referência de Coordenada (Coordinate reference system)__ clicar no botão __Selecionar SRC (Select CRS)__:

![](imagens/txtdelimitado_08.png)

Na tela que abrir, basta procurar o SRC nas listagens, ou digitando em Filtro:

![](imagens/txtdelimitado_09.png)

O Qgis permite apenas a leitura de arquivos de texto delimitados, caso precise editar algum, é necessário salvar salvar como algum arquivo vetorial para fazer tal edição.  
Para isso, basta clicar sobre a camada com o botão direito ir em __Salvar Como (Save As)__:

![](imagens/txtdelimitado_10.png)

Na janela que abrir, basta configurar o arquivo de saida.  

![](imagens/txtdelimitado_11.png)  

Em __Formato (Format)__ escolhemos o formato de saída.  
Em __Nome do Arquivo (File Name)__ Navegamos até a pasta onde queremos que o arquivo seja salvo, e damos um nome a ele.  
Em __Nome da camada (Layer Name)__ escolhemos um nome para a camada se ela for adicionada de volta ao projeto apos salva.  
Em __Codificação (Encoding)__ podemos escolher a codificação da tabela de atributos do arquivo de saida.  
Em __Salvar apenas feições selecionadas (Save only selected features))__ caso tenhamos selecionado alguma feição dentro do nosso arquivo e queremos apenas ela no arquivo novo, caso contrario, desmarca essa opção. ela só será habilitada caso haja alguma feição selecionada.  
Em __Selecionar campos para expotar e suas opções de exportação (Select fields to export and their export options)__ podemos escolher os campos a serem exportados.
