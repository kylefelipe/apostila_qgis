# Adicionando arquivos ao Projeto

O Qgis, como já dito, nos permite trabalhar com diversos formatos de arquivos.  
Para trabalharmos eles, precisamos adicionar ao projeto os arquivos necessários.

## Arquivos Vetoriais:

_Atenção_: Como base, vamos sempre utilizar arquivos vetoriais em formato GEOPACKAGE, mas vamos mostrar um pouco a utilização do _ESRI_ _SHAPEFILE_.

Para adicionarmos dados vetoriais no Qgis, temos algumas formas:

![](imagens/addarquivos_01.png)

Botão _Adicionar camada vetorial_ (1) e menu _`Camada (layer)`_>_`Adicionar camada (Add Layer)`_ > _`Adicionar Camada Vetorial`_ (2) bem como a tecla de atalho _Ctrl+Shift+V_.

![](imagens/addarquivos_02.png)

1.  _Tipo da Fonte (Source Type)_: Aqui escolhemos o tipo de formato que vamos abrir, arquivo, diretório, banco de dados, protocolo.  
 Vamos deixar o arquivo marcado.

2.  _Codificação (Coding)_: Aqui, escolhemos a codificação da tabela de atributos, principalmente em formatos que não tem essa informação neles, como o SHAPEFILE.

3.  _Fonte (source)_: Aqui navegamos até a fonte do dado que queremos.

Ao clicar em _Navegar (Browse)_ na seguinte tela podemos navegar até a pasta onde encontra os arquivos que queremos abrir.

![](imagens/addarquivos_03.png)

__Dica!__: Alguns formatos são formados por diversos arquivos, podemos utilizar o filtro de arquivo para encontrar com facilidade o que queremos abrir.

__Dica!__: Alguns formatos podem conter mais de uma camada vetorial, o qgis irá solicitar qual camada deseja inserir no projeto. Basta selecionar as camadas desejadas.

Um projeto com camadas vetoriais adicionadas fica com a aparência assim:

![](imagens/addarquivos_04.png)

Temos de ter em mente que a ordem das camadas no painel de camadas importa na hora de renderizar (desenhar) os dados na tela, ou seja, os dados que estão mais em cima são desenhados por cima, aqui vai uma dica para manter a organização das camadas na seguinte ordem:
*   Pontos;  
*   Linhas;  
*   Poligonos;  
*   Rasters;  

Sim, eu sei, ainda nem falei em como adicionar os rasters ainda... mas \#FiqueFirme.  

Podemos reorganizar os dados no painel arrastando eles para cima ou para baixo.

\- Mas como saber se é um arquivo de ponto, linha ou poligono?
\- Boa pergunta, jovem gafanhoto...

Observe no painel de camadas, há um desenho entre o nome da camada e a caixa:

![](imagens/addarquivos_05.png)

Esse desenho indica o tipo de dado da camada:

![](imagens/addarquivos_06.png): Ponto  
![](imagens/addarquivos_07.png): Linha  
![](imagens/addarquivos_08.png): Poligono  
![](imagens/addarquivos_09.png): Raster

Agora que já sabe identificar as camadas que foram adicionadas ao projeto, podemos mudar a ordem delas e testar como o Qgis renderiza as camadas.

## Dados tabulares:  

Para adicionar dados tabeulares no QGIs vai depender da forma que estão sendo distribuidos.  

### Planilhas:

Para adicionar planilhas ao QGIS basta adicionar como se fossem arquivos vetoriais normais. Lembre de retirar o filtro de arquivo antes e de configurar a codificação correta para o arquivo a ser aberto.

Caso o arquivo contenha mais de uma planilha, o qgis irá solicita a escolha de quais tabelas devem se abertas.

### CSV:

Para adicionar aquivos tabulares em CSV, ou outro arquivo de texto delimitado, temos algumas formas:  

![](imagens/addarquivos_12.png)  

Botão _Adicionar Camada de Texto Delimitado_ (1), menu _`Camada (layer)`_ > _`Adicionar camada (Add Layer)`_ > _`Adiconar Camada de texto delimitado (Add Demited Text Layer)`_ (2). Não há tecla de atalho, mas, o qgis é customisável, lembra?!

Na janela que abrir, basta navegar até o arquivo desejado em __Nome do Arquivo (File Name)__ e depois configurar para que o arquivo apareça corretamente:

![](imagens/addarquivos_13.png)  

Como estamos adicionando apenas um dado tabular, sem geometria, precisamos marcar a opção _Sem geometria (No geometry)_, veremos depois como adicionar arquivos delimitados com geometria.  

\- E como eu identifico que a minha camada é um dado tabular?
\- Muito bem, padawan, dados tabulares aparecem da seguinte forma no painel de camadas:  

![](imagens/addarquivos_12.png)  

## Arquivos Matriciais (Rasters):

_Atenção!:_ Algumas imagens de satélite precisam de alguns procedimentos antes de serem adicionadas ao projeto, isso será tratado em um capitulo especial sobre dados matriciais.

Para adicionarmos arquivos Rasters ao Qgis, temos algumas formas:  

![](imagens/addarquivos_10.png)  

Botão _Adicionar camada raster_ (1), menu _`Camada (layer)`_ > _`Adicionar camada (Add Layer)`_ > _`Adiconar Camada Raster (Add Raster Layer)`_ (2), bem como com a tecla de atalho _Ctrl+Shift+R_.  
E na janela que abrir, basta navegar até o arquivo desejado.

![](imagens/addarquivos_11.png)  

Aqui também podemos fazer filtragem de arquivos para facilitar.

## Banco de dados:  

Esse fica para um capitulo exclusivo também.
