# Configurando o QGIS.  

O Qgis oferece diversas formas de customização, que podem atuar a nivel global, no software como um todo, ou apenas no projeto que está trabalhando.  
Aqui vamos tratar apenas das configuraçoẽs globais, as de projeto ficará para um tópico específico sobre o projeto.  

## Configurações Globais: ##

As configurações feitas aqui reverberam no software como um todo,e nos projetos, novos e preexistentes.  
Aqui podemos customizar Sistemas de Referencia de Coordenadas, Cores basicas utilizadas no projeto, teclas de atalho, aderencia das camadas (snapping), idioma, proxy e outras customizações.

Tudo isso utilizando o menu _`Configurações`_ (_`Settings`_)

![](imagens/configglobal_01.PNG)

Vamos tratar de algumas configurações para nos ajudar na produção, acessando  
_`Configurações > Opções`_ (_`Settings > Options`_)  

![](imagens/configglobal_02.png)  

Aqui temos:  

1.  Painel contendo as abas.
2.  Tela de configuração da aba.
Para mudar entre as abas, basta clicar nos botão desejado no painel.  
Há também os botões de __Ajuda__, __Cancelar__ e __OK__, a posição deles varia de acordo com o Sistema Operacional que estiver usando.

#### Configurações > Opções > Gerais (General): ####

![](imagens/configglobal_03.png)  

Nessa aba, em _Aplicação_ (_Application_), podemos configurar a aparência do Qgis o tema da interface, a fonte padrão utilizada, tamanho dos icones, das fontes, o tempo de duração das mensagens que aparecem no Qgis.  

Em _Arquivos de Projeto_ (_Project files_), podemos configurar se queremos que alguma tela de boas vindas, ou nenhuma, um padrão para novos projetos, e a possibilidade de utilzar macros.

#### Configurações > Opções > Sistema (System): ####

![](imagens/configglobal_04.png)  


Aqui devemos terbastante atenção ao mudar alguma configuração, pois aqui podemos configurar os caminhos para pastas contendo SVG, caminhos para plugins, resetar configurações feitas pelo usuário, e configurar variáveis de ambiente.

#### Configurações > Opções > Fontes de Dados (Data Sources): ####

![](imagens/configglobal_05.png)  

Aqui configuramos, a forma de como a tabela de atributos abre, a forma de cópia dos dados (quando se manda copiar um dado tabular ou vetorial), comportamento da tabela de atributos (se abre todas as feições, apenas selecionados...), dentre outras.  

#### Configurações > Opções > Renderização (Rendering): ####

![](imagens/configglobal_06.png)  

Aqui vamos configurar como comporta a renderização dos dados no QGIS, quantidade de núcleos utilizados para isso, qualidade e por ai vai.  


#### Configurações > Opções > SRC (CRS): ####

![](imagens/configglobal_07.png)  

Aqui vamos controlar qual o SRC em projetos novos, o comportamento quando uma camada não possui um SRC Definido e até mesmo transformações a serem feitas quando há SRCs diferentes.  

#### Configurações > Opções > Localização (Locale): ####

![](imagens/configglobal_08.png)  

Lembra que anteriormente foi falado da questão do problema da tradução para Português do Brasil?
Então, é aqui que vamos tratar isso, bastar marcar a caixa "Sobrepor localização do sistema" e na lista escolher U.S English, é necessário reiniciar o Qgis, vamos fazer isso no final desse capitulo.  

#### Configurações > Opções > Rede (Network): ####

![](imagens/configglobal_09.png)  

Aqui podemos configurar as especificidades da rede, principalmente quando estamos em ambientes corporativos, que precisam de proxy para acesso a internet.

Muito bom... Agora, pode reiniciar o qgis para que as configurações de idioma comecem a valer.

#### Configurações > Atalhos ####

Acessando o menu _`Configurações > Atalhos (Settings > Shortcuts)`_ podemos configurar as teclas de atalhos para comandos, algoritimos e plugins.

![](imagens/configglobal_10.png)

Basta procurar na lista o que deseja inserir/alterar o atalho, clicar em __Mudar__ e teclar o comando desejado.
Caso desejar utilizar o mesmo comando em várias maquinas, para não ter que ficar configurando cada atalho em todas elas, utilize o botão _Salve__ __(Save)__ para salvar um arquivo XML com todos os atalhoes, e na maquina que desejar basta utilizar o botão __Load__ para carregar o arquivo de atalhos customizados.
